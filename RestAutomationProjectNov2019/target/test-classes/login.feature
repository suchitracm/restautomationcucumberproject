Feature: Testing a Rest API
User should be able to submit GET, POST, PUT and DELETE request to the Landlords.

Scenario: User should be able to submit GET request to Landlord.
Given : User navigate to Landlords
When : User submit GET request of Landlords
Then : User should be able to see the list of Landlords.

Scenario: User should be able to submit POST request to Landlord.
Given : User navigate with given data with post to Landlords
When : User submit POST request of Landlords by entering first name and last name
Then : User should be able to add the Landlords

Scenario: User should be able to submit PUT request to Landlord.
Given : User navigate with given data with put to Landlords
When : User submit PUT request of Landlords by entering first name and last name.
Then : User should be able to update the Landlord.

Scenario: User should be able to submit DELETE request to Landlord.
Given : User navigate with given data with delete to Landlords
When : User submit DELETE request of Landlords with first name and last name
Then : User should be able to delete the Landlord.




