package com.rest.RestAutomationProject.javacodes;

import static com.jayway.restassured.RestAssured.given;

import org.testng.annotations.Test;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ValidatableResponse;
import com.jayway.restassured.specification.RequestSpecification;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinition  {
//	LandlordTest lord = new LandlordTest();
	public Response response;
	public RequestSpecification request;
	public ValidatableResponse json;
	
	@Test( priority =1)
	@Given("^: User navigate to Landlords$")
	public void user_navigate_to_Landlords() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
System.out.println("To verify the list of the user");
System.out.println("The Get request Given block:");
request = given();
}

	@Test( priority =2)
	@When("^: User submit GET request of Landlords$")
	public void user_submit_GET_request_of_Landlords() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("The Get request When block: ");
		response = request.when().get("http://localhost:8080/landlords");
		System.out.println(response.prettyPrint());
	}


	
	
	@Test( priority =3)
	@Then("^: User should be able to see the list of Landlords\\.$")
	public void user_should_be_able_to_see_the_list_of_Landlords() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   //lord.getLandlord();
		//json = response.then().statusCode(200);
		System.out.println("The Get request Then block:");
		response.then().extract().response().prettyPrint();
		
		
	}
	
	

	Landlord LL=new Landlord("Ravi444","tester444",true);

	@Test( priority =4)
	@Given("^: User navigate with given data with post to Landlords$")
	public void user_navigate_with_given_data_with_post_to_Landlords() throws Throwable {
System.out.println("To verify the Post request of the user");
System.out.println("The Post request Given block:");
request = given().contentType(ContentType.JSON);
request =  given().body(LL); 

}	

	@Test( priority =5)
@When("^: User submit POST request of Landlords by entering first name and last name$")
public void user_submit_POST_request_of_Landlords_by_entering_first_name_and_last_name() throws Throwable {
    // Write code here that turns the phrase above into concrete actions

System.out.println("The Post request When block:");
response=request.when().post("http://localhost:8080/landlords");
}

	@Test( priority =6)
@Then("^: User should be able to add the Landlords$")
public void user_should_be_able_to_add_the_Landlords() throws Throwable {
System.out.println("The Post method Then block.......");

//json=response.then().statusCode(200);
//System.out.println(json+".........sjson");

String str=given()
.contentType(ContentType.JSON)
.body(LL)
.when()
.post("http://localhost:8080/landlords")
.then()
.extract().response().prettyPrint();
System.out.println("Starting");
System.out.println(str+" str");
JsonPath path=new JsonPath(str);
String ID=path.getString("id");
System.out.println(ID + "    ID");
String ftName = path.getString("firstName");
System.out.println(ftName);
System.out.println("Verifying the Landlord Added");
request.given().pathParam("id", ID).when().get("http://localhost:8080/landlords/{id}").then().extract().response().prettyPrint();

}

@Test( priority =7)
@Given("^: User navigate with given data with put to Landlords$")
public void user_navigate_with_given_data_with_put_to_Landlords() throws Throwable {
System.out.println("To verify the Put request of the user");
System.out.println("The Put request Given block:");
request = given().contentType(ContentType.JSON);
request =  given().body(LL); 

}	
	

@Test( priority =8)
@When("^: User submit PUT request of Landlords by entering first name and last name\\.$")
public void user_submit_PUT_request_of_Landlords_by_entering_first_name_and_last_name() throws Throwable {
	System.out.println("The Put request When block............");
	response=request.when().post("http://localhost:8080/landlords");

}

@Test( priority =9)
@Then("^: User should be able to update the Landlord\\.$")
public void user_should_be_able_to_update_the_Landlord() throws Throwable {
	System.out.println("The Put request Then block.............."); 
	Landlord LL1=new Landlord("Rass","tester",true);

	String str=given()
			.contentType(ContentType.JSON)
			.body(LL1)
			//POJO class - Serialization and de-serialization
			.when()
			.post("http://localhost:8080/landlords")
			.then()
//			.statusCode(201)
			.extract().response().body().prettyPrint();
			JsonPath path=new JsonPath(str);
			String ID=path.getString("id");
			//String ftName = path.getString("firstName");
			System.out.println(ID);
			
			Landlord LL2=new Landlord("Rass123","tester",true);
			try {
			given()
		.contentType(ContentType.JSON)
		.body(LL2)
		.pathParam("id", ID)
		.when()
		.put("http://localhost:8080/landlords/{id}")
		.then()

		.statusCode(200)



		.extract().response().prettyPrint();
			
			}
			catch (Exception e)
			{
				e.getMessage();
			}
		}

@Test( priority =10)
@Given("^: User navigate with given data with delete to Landlords$")
public void user_navigate_with_given_data_with_delete_to_Landlords() throws Throwable {
System.out.println("To verify the Delete request of the user");
System.out.println("The Delete request Given block:");
request = given().contentType(ContentType.JSON);
request =  given().body(LL); 

}	

@Test( priority =11)
@When("^: User submit DELETE request of Landlords with first name and last name$")
public void user_submit_DELETE_request_of_Landlords_with_first_name_and_last_name() throws Throwable {
	System.out.println("The Delete request When block.............."); 
	response=request.when().post("http://localhost:8080/landlords");
	
    
}

@Test( priority =12)
@Then("^: User should be able to delete the Landlord\\.$")
public void user_should_be_able_to_delete_the_Landlord() throws Throwable {
	System.out.println("The Delete request Then block.............."); 
	Landlord LL=new Landlord("Razz","tester",true);
	String str=given()
		.contentType(ContentType.JSON)
		.body(LL)
		//POJO class - Serialization and de-serialization
		.when()
		.post("http://localhost:8080/landlords")
		.then()
		//.statusCode(201)
		.extract().response().body().prettyPrint();
		JsonPath path=new JsonPath(str);
		String ID=path.getString("id");
		//String ftName = path.getString("firstName");
		System.out.println(ID);
		
	given()
	.contentType(ContentType.JSON)
	.pathParam("id", ID)
	.when()
	.delete("http://localhost:8080/landlords/{id}")
	.then()
	//.statusCode(200)
	.extract().response().body().prettyPrint();
   
}


	
	
	
	
	
	
	

}
