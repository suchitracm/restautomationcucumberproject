package com.rest.RestAutomationProject.javacodes;

public class Landlord {
private String firstName;
private String lastName;
private boolean trusted;

public Landlord()
{
	
}

public Landlord(String FN,String LN,Boolean trus)
{
	this.firstName=FN;
	this.lastName=LN;
	this.trusted=trus;
	
}

public String getFirstName()
{
	return firstName;
}

public void setFirstName(String firstName)
{
	this.firstName=firstName;
}

public String getLastName()
{
	return lastName;
}
public void setLastName(String lastName)
{
	this.lastName=lastName;
}

public Boolean gettrusted()
{
	return trusted;
}
public void setTrusted(Boolean trusted)
{
	this.trusted=trusted;
}
}
