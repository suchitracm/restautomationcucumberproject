package com.rest.RestAutomationProjectNov2019;
import org.testng.annotations.Test;
import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matcher.*;
import com.jayway.restassured.path.json.JsonPath;


public class SampleProject {
	
@Test
public void sampleOne()
{
given()
.pathParam("id",1)
.when()
.get("https://jsonplaceholder.typicode.com/posts/{id}")
.then()
.statusCode(200)
.extract().response().prettyPrint();

}

}
