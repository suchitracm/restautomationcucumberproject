package com.rest.RestAutomationProjectNov2019;

import org.junit.runner.RunWith;
//import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="D:\\TechArch\\RestAutomationCucumberProject\\RestAutomationProjectNov2019\\src\\test\\resources\\login.feature",
glue= {"com.rest.RestAutomationProject.javacodes"},
plugin = {
"com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html",
"pretty", "html:target/cucumber", "json:target/cucumber.json"})
//"com.cucumber.listener.ExtentCucumberFormatter:test-out/report.html"}monochrome = true) extends AbstractTestNGCucumberTests
public class Runner
{

	}